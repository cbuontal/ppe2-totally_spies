#!/usr/bin/python3
#! *-* coding:utf-8 -*

import argparse
import feedparser

parser = argparse.ArgumentParser(
                    prog="extraire_flux_feedparser.py",
                    description="Extrait le texte d'un fichier XML")

parser.add_argument("xml_file")
args = parser.parse_args()

# le nom du fichier est accessible par args.xml_file

chemin = "../corpus/" + args.xml_file

def methode_feedparser(path):

    d = feedparser.parse(path)

    for i in range(len(d.entries)):
        print("Article ", i+1, " :")
        print(d.entries[i]['title'])
        print(d.entries[i]['description'])
        print()

if if __name__ == '__main__':
    methode_feedparser(chemin)
