#! /bin/python
# -*- coding:utf-8 -*

from typing import List, Dict
from datastructures import Token
from trankit import Pipeline

def remplir_token(description: str) -> List[Token]:
    """
        Renvoie une liste de token à partir du contenu d'un article (description)
        L'objet token contient :
         - forme
         - lemme
         - POS
    """
    p = Pipeline('french')

    liste_token = []

    pos_tagging = p.posdep(description)
    lemmatization = p.lemmatize(description)

    for (id_sentence, sentence) in enumerate(pos_tagging['sentences']):
        # on explore le dictionnaire, on extrait forme et lemme
        for tokens in sentence['tokens']:
            # liste de tokens
            forme = tokens['text']
            lemme = ""
            upos = tokens['upos']

            # on va ajouter le lemme
            # si IndexError, on garde lemme = forme
            try:
                lemme = lemmatization['sentences'][sentence['id']]['tokens'][tokens['id']]['lemma']
            except IndexError:
                lemme = forme

            # rang dans la liste = index - 1
            gouverneur = sentence['tokens'][tokens['head']-1]['text']
            deprel = tokens['deprel']

            token = Token(forme, lemme, upos, gouverneur, deprel)

            if token not in liste_token:
                liste_token.append(token)

    return liste_token
