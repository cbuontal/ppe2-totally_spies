#!/usr/bin/python3
# -*- coding:utf-8 -*

import os, feedparser, html
from pathlib import Path
import xml.etree.ElementTree as ET
import pickle

def main():
    dico_cat={"une":"0,2-3208,1-0,0",
              "international":"0,2-3210,1-0,0",
              "europe":"0,2-3214,1-0,0",
              "societe":"0,2-3224,1-0,0",
              "idees":"0,2-3232,1-0,0",
              "economie":"0,2-3234,1-0,0",
              "actualite-medias":"0,2-3236,1-0,0",
              "sport":"0,2-3242,1-0,0",
              "planete":"0,2-3244,1-0,0",
              "culture":"0,2-3246,1-0,0",
              "livres":"0,2-3260,1-0,0",
              "cinema":"0,2-3476,1-0,0",
              "voyage":"0,2-3546,1-0,0",
              "technologies":"0,2-651865,1-0,0",
              "politique":"0,57-0,64-823353,0",
              "sciences":"env_sciences"}

    # correspondance dans l'autre sens :
    dico_cat2 = {}
    for key, value in dico_cat.items():
        dico_cat2[value] = key

    dic_mois = {
        "01": "Jan",
        "02": "Feb",
        "03": "Mar",
        "04": "Apr",
        "05": "May",
        "06": "Jun",
        "07": "Jul",
        "08": "Aug",
        "09": "Sep",
        "10": "Oct",
        "11": "Nov",
        "12": "Dec"}

    # dans l'autre sens
    dic_mois_reverse = {}
    for key, value in dic_mois.items():
        dic_mois_reverse[value] = key

    choix = int(input("Rechercher par date (1) ou catégorie (2) ? "))

    # quel que soit le choix, on va exporter dans un fichier xml
    root = ET.Element('root')

    # ************************ PAR DATE ****************************************

    if choix == 1:
        articles = []
        categories = []
        date = input("Entrez la date voulue : (format DD/MM/YYYY) ")

        (jour, mois, annee) = date.split('/')
        mois = dic_mois[mois]
        date = annee + '/' + mois + '/' + jour
        chemin = "/Users/mak/Desktop/TAL/PPE2/PPE2/" + date

        if Path(chemin).is_dir():
            chemins = sorted(Path(chemin).glob('**/*.xml'))
            # pour chaque chemin vers un fichier XML, on extrait le dernier
            # élément après '/', qui contient le code. Le dictionnaire dico_cat2
            # donne la correspondance en langage naturel
            for chemin in chemins:
                # on garde le dernier élément de chemin
                # et on garde ce qu'il y a avant le point (ABC.xml)
                nom_fichier = str(chemin).split('/')[-1].split('.')[0]
                # si c'est bien une catégorie, on continue
                if nom_fichier in dico_cat2.keys():
                    # pour chaque fichier, on récupère sa catégorie :
                    categorie = dico_cat2[nom_fichier]
                    # affichage exo 2
                    #print(f"******************** CATEGORIE : {categorie} ********************")
                    d = feedparser.parse(chemin)

                    for i in range(len(d.entries)):
                        article = {}
                        article["date"] = jour + '/' + dic_mois_reverse[mois] + '/' + annee
                        article["categorie"] = categorie
                        article["titre"] = d.entries[i]['title']
                        article["description"] = d.entries[i]['description']
                        articles.append(article)

            nom_fichier_sortie = input("Donnez un nom du fichier en sortie : ")
            if not nom_fichier_sortie.endswith(".pickle"):
                nom_fichier_sortie += ".pickle"
            with open(nom_fichier_sortie, "wb") as f:
                pickle.dump(articles, f)

        else:
            print("Aucun dossier ne correspond à cette date")

    # **************************** PAR CATEGORIE *******************************

    if choix == 2:
        categories = []
        articles = []
        categories = input("Quelles catégories ? (séparées par un espace) ").split()
        # on ne garde que ce qui existe dans le dictionnaire des catégories
        categories = [cat for cat in categories if cat in dico_cat.keys()]
        print(categories)

        # on s'assure qu'on ait au moins une catégorie
        if len(categories) > 0:

            for categorie in categories:
                chemin_cat = "/Users/mak/Desktop/TAL/PPE2/PPE2/2022/"
                chemins_xml = sorted(Path(chemin_cat).glob('**/' + dico_cat[categorie]+ ".xml"))
                for chemin in chemins_xml:
                    # pour chaque fichier, on récupère sa catégorie :
                    categorie = dico_cat2[str(chemin).split('/')[-1].split('.')[0]]
                    d = feedparser.parse(chemin)
                    for i in range(len(d.entries)):
                        article = {}
                        article["date"] = d.entries[i]['published']
                        article["categorie"] = categorie
                        article["titre"] = d.entries[i]['title']
                        article["description"] = d.entries[i]['description']
                        articles.append(article)

            nom_fichier_sortie = input("Donnez un nom du fichier en sortie : ")

            if not nom_fichier_sortie.endswith(".pickle"):
                nom_fichier_sortie += ".pickle"
            with open(nom_fichier_sortie, "wb") as f:
                pickle.dump(articles, f)

        else:
            print("Aucune catégorie valide sélectionnée")

    choice = input("Voulez vous afficher le résultat : 'Oui' ou 'Non' ? ")
    if choice == 'Oui':
        # Charger le contenu du fichier dans une variable
        with open(nom_fichier_sortie, "rb") as f:
            obj = pickle.load(f)
            # Afficher l'objet chargé
            print(obj)
            for article in obj:
                article['titre'] = article['titre'].replace('\xa0', ' ')
                article['description'] = article['description'].replace('\xa0', ' ')
                print("Date :", article["date"])
                print("Catégorie :", article["categorie"])
                print("Titre :", article["titre"])
                print("Description :", article["description"])
                print("\n")
    else:
        print('Ok')

if __name__ == '__main__':
    main()
