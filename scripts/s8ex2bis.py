#!/usr/bin/python3
# -*- coding:utf-8 -*

import os, feedparser, html
from pathlib import Path
import xml.etree.ElementTree as ET

def main():
    dico_cat={"une":"0,2-3208,1-0,0",
              "international":"0,2-3210,1-0,0",
              "europe":"0,2-3214,1-0,0",
              "societe":"0,2-3224,1-0,0",
              "idees":"0,2-3232,1-0,0",
              "economie":"0,2-3234,1-0,0",
              "actualite-medias":"0,2-3236,1-0,0",
              "sport":"0,2-3242,1-0,0",
              "planete":"0,2-3244,1-0,0",
              "culture":"0,2-3246,1-0,0",
              "livres":"0,2-3260,1-0,0",
              "cinema":"0,2-3476,1-0,0",
              "voyage":"0,2-3546,1-0,0",
              "technologies":"0,2-651865,1-0,0",
              "politique":"0,57-0,64-823353,0",
              "sciences":"env_sciences"}

    # correspondance dans l'autre sens :
    dico_cat2 = {}
    for key, value in dico_cat.items():
        dico_cat2[value] = key

    dic_mois = {
        "01": "Jan",
        "02": "Feb",
        "03": "Mar",
        "04": "Apr",
        "05": "May",
        "06": "Jun",
        "07": "Jul",
        "08": "Aug",
        "09": "Sep",
        "10": "Oct",
        "11": "Nov",
        "12": "Dec"}

    # dans l'autre sens
    dic_mois_reverse = {}
    for key, value in dic_mois.items():
        dic_mois_reverse[value] = key

    choix = int(input("Rechercher par date (1) ou catégorie (2) ? "))

    # quel que soit le choix, on va exporter dans un fichier xml
    root = ET.Element('root')

    # ************************ PAR DATE ****************************************
    if choix == 1:
        date = input("Entrez la date voulue : (format DD/MM/YYYY) ")

        (jour, mois, annee) = date.split('/')
        mois = dic_mois[mois]
        date = annee + '/' + mois + '/' + jour
        chemin = "../../" + date

        if Path(chemin).is_dir():
            chemins = sorted(Path(chemin).glob('**/*.xml'))
            # pour chaque chemin vers un fichier XML, on extrait le dernier
            # élément après '/', qui contient le code. Le dictionnaire dico_cat2
            # donne la correspondance en langage naturel
            for chemin in chemins:
                # on garde le dernier élément de chemin
                # et on garde ce qu'il y a avant le point (ABC.xml)
                nom_fichier = str(chemin).split('/')[-1].split('.')[0]
                # si c'est bien une catégorie, on continue
                if nom_fichier in dico_cat2.keys():
                    # pour chaque fichier, on récupère sa catégorie :
                    categorie = dico_cat2[nom_fichier]
                    # affichage exo 2
                    #print(f"******************** CATEGORIE : {categorie} ********************")
                    d = feedparser.parse(chemin)

                    for i in range(len(d.entries)):
                        # écriture dans un fichier XML (2bis)
                        article = ET.SubElement(root, "article")
                        # enfants de l'élément 'article'
                        date_art = ET.SubElement(article, "date")
                        date_art.text = jour + '/' + dic_mois_reverse[mois] + '/' + annee
                        categorie_art = ET.SubElement(article, 'categorie')
                        categorie_art.text = categorie
                        titre_art = ET.SubElement(article, 'titre')
                        titre_art.text = d.entries[i]['title']
                        description_art = ET.SubElement(article, "description")
                        description_art.text = d.entries[i]['description']

            # une fois la boucle finie, on construit l'arbre
            tree = ET.ElementTree(root)
            # on l'écrit dans un fichier
            nom_fichier_sortie = input("Donnez un nom du fichier XML en sortie : ")
            if not nom_fichier_sortie.endswith(".xml"):
                nom_fichier_sortie += ".xml"
            tree.write(nom_fichier_sortie, encoding="UTF-8", xml_declaration=True)

        else:
            print("Aucun dossier ne correspond à cette date")

    # **************************** PAR CATEGORIE *******************************
    if choix == 2:
        categories = []
        categories = input("Quelles catégories ? (séparées par un espace) ").split()
        # on ne garde que ce qui existe dans le dictionnaire des catégories
        categories = [ cat for cat in categories if cat in dico_cat.keys() ]
        print(categories)

        # on s'assure qu'on ait au moins une catégorie
        if len(categories) > 0:
            # pour chaque catégorie on sélectionne les fichiers dont le code correspond
            for cat in categories:
                # liste de tous les chemins vers les fichiers
                print(f"******************** CATEGORIE : {cat} ********************")
                chemins = sorted(Path('../../2022').glob('**/'+dico_cat[cat]+'.xml'))
                #for x in chemins:
                #    print(x)
                # on appelle la méthode feedparser sur chaque fichier
                for chemin in chemins:
                    # on affiche la date du chemin :
                    date = str(chemin).split('/')

                    index_annee = date.index('2022')
                    #print(f"DATE : {date[index_annee+2]} {date[index_annee+1]} 2022")
                    date_str = date[index_annee+2] + '/' + dic_mois_reverse[date[index_annee+1]] + '/' + "2022"

                    d = feedparser.parse(chemin)

                    for i in range(len(d.entries)):
                        # écriture dans un fichier XML (2bis)
                        article = ET.SubElement(root, "article")
                        # enfants de l'élément 'article'
                        date_art = ET.SubElement(article, "date")
                        date_art.text = date_str
                        categorie_art = ET.SubElement(article, 'categorie')
                        categorie_art.text = cat # catégorie courante du tour de boucle
                        titre_art = ET.SubElement(article, 'titre')
                        titre_art.text = d.entries[i]['title']
                        description_art = ET.SubElement(article, "description")
                        description_art.text = d.entries[i]['description']

            # à la sortie de la boucle, on peut faire l'arbre et exporter
            tree = ET.ElementTree(root)
            # on l'écrit dans un fichier
            nom_fichier_sortie = input("Donnez un nom du fichier XML en sortie : ")
            if not nom_fichier_sortie.endswith(".xml"):
                nom_fichier_sortie += ".xml"
            tree.write(nom_fichier_sortie, encoding="UTF-8", xml_declaration=True)

        else:
            print("Erreur : aucune catégorie n'est valide.")

if __name__ == '__main__':
    main()
