#!/usr/bin/python3
# -*- coding:utf-8 -*

import os
from pathlib import Path
from s6ex1 import methode_feedparser

def main():
    dico_cat={"une":"0,2-3208,1-0,0",
              "international":"0,2-3210,1-0,0",
              "europe":"0,2-3214,1-0,0",
              "societe":"0,2-3224,1-0,0",
              "idees":"0,2-3232,1-0,0",
              "economie":"0,2-3234,1-0,0",
              "actualite-medias":"0,2-3236,1-0,0",
              "sport":"0,2-3242,1-0,0",
              "planete":"0,2-3244,1-0,0",
              "culture":"0,2-3246,1-0,0",
              "livres":"0,2-3260,1-0,0",
              "cinema":"0,2-3476,1-0,0",
              "voyage":"0,2-3546,1-0,0",
              "technologies":"0,2-651865,1-0,0",
              "politique":"0,57-0,64-823353,0",
              "sciences":"env_sciences"}

    # correspondance dans l'autre sens :
    dico_cat2 = {}
    for key, value in dico_cat.items():
        dico_cat2[value] = key

    choix = int(input("Rechercher par date (1) ou catégorie (2) ? "))

    if choix == 1:
        date = input("Entrez la date voulue : (format DD/MM/YYYY) ")
        dic_mois = {
            "01": "Jan",
            "02": "Feb",
            "03": "Mar",
            "04": "Apr",
            "05": "May",
            "06": "Jun",
            "07": "Jul",
            "08": "Aug",
            "09": "Sep",
            "10": "Oct",
            "11": "Nov",
            "12": "Dec"}
        (jour, mois, annee) = date.split('/')
        mois = dic_mois[mois]
        chemin = "../../" + annee + '/' + mois + '/' + jour

        if Path(chemin).is_dir():
            chemins = sorted(Path(chemin).glob('**/*.xml'))
            # pour chaque chemin vers un fichier XML, on extrait le dernier
            # élément après '/', qui contient le code. Le dictionnaire dico_cat2
            # donne la correspondance en langage naturel
            for chemin in chemins:
                # on garde le dernier élément de chemin
                # et on garde ce qu'il y a avant le point (ABC.xml)
                nom_fichier = str(chemin).split('/')[-1].split('.')[0]
                # si c'est bien une catégorie, on continue
                if nom_fichier in dico_cat2.keys():
                    categorie = dico_cat2[nom_fichier]
                    print(f"******************** CATEGORIE : {categorie} ********************")
                    methode_feedparser(chemin)
        else:
            print("Aucun dossier ne correspond à cette date")


    if choix == 2:
        categories = []
        categories = input("Quelles catégories ? (séparées par un espace) ").split()
        # on ne garde que ce qui existe dans le dictionnaire des catégories
        categories = [ cat for cat in categories if cat in dico_cat.keys() ]
        print(categories)

        # pour chaque catégorie on sélectionne les fichiers dont le code correspond
        for cat in categories:
            # liste de tous les chemins vers les fichiers
            print(f"******************** CATEGORIE : {cat} ********************")
            chemins = sorted(Path('../../2022').glob('**/'+dico_cat[cat]+'.xml'))
            for x in chemins:
                print(x)
            # on appelle la méthode feedparser sur chaque fichier
            for chemin in chemins:
                # on affiche la date du chemin :
                date = str(chemin).split('/')
                index_annee = date.index('2022')
                print(f"DATE : {date[index_annee+2]} {date[index_annee+1]} 2022")
                methode_feedparser(chemin)

if __name__ == '__main__':
    main()
