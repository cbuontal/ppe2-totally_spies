import re
import pathlib

def extract_titles_and_descriptions_from_xml_file(xml_file_path):
    xml = pathlib.Path(xml_file_path).read_text()

    items = re.findall(r'<item>.*?</item>', xml)

    for item in items:
        title = re.findall(r'<title>.*?</title>', item)[0].replace('<![CDATA[', '').replace(']]>', '')
        description = re.findall(r'<description>.*?</description>', item)[0].replace('<![CDATA[', '').replace(']]>', '')

        print(f'Title: {title}\nDescription: {description}\n')

extract_titles_and_descriptions_from_xml_file('/Users/mak/Desktop/TAL/PPE2/PPE2/sample.xml')
