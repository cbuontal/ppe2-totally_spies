import pickle
import pprint

def lire_fichier_pickle(fichier):
    with open(fichier, 'rb') as file:
        obj = pickle.load(file)
    return obj

# Lire le fichier pickle
obj = lire_fichier_pickle('/Users/mak/PycharmProjects/PPE2/output.pickle')

# Afficher l'objet de manière lisible
pprint.pprint(obj)
