import re
import pathlib

def extraire_xml_file(xml_file_path):
    xml = pathlib.Path(xml_file_path).read_text()

    items = re.findall(r'<item>.*?</item>', xml)

    for item in items:
        pubdate = re.findall(r'(?<=<pubDate>).*?(?=<\/pubDate>)', item)
        title = re.findall(r'(?<=<title>).*?(?=<\/title>)', item)[0].replace('<![CDATA[', '').replace(']]>', '')
        description = re.findall(r'(?<=<description>).*?(?=<\/description>)', item)[0].replace('<![CDATA[', '').replace(']]>', '')

        item = {'date': pubdate, 'title': title, 'description': description}
        yield item

def sort_items(xml_file_path):
    items = list(extraire_xml_file(xml_file_path))
    sorted_items = sorted(items, key=lambda x: x['date'])

    for item in sorted_items:
        print(f"Date: {item['date']}\nTitle: {item['title']}\nDescription: {item['description']}\n")

xml_file_path = '/Users/mak/Desktop/TAL/PPE2/PPE2/sample.xml'
sort_items(xml_file_path)
