#!/usr/bin/python3
#! *-* coding:utf-8 -*

import argparse
import feedparser

def methode_feedparser(path):
    """
        Prend en entrée le chemin vers le fichier XML (str)
        Affiche les titres et les descriptions de chaque article
    """

    d = feedparser.parse(path)

    for i in range(len(d.entries)):
        print("Article ", i+1, " :")
        print("Titre : " + d.entries[i]['title'])
        print("Description : " + d.entries[i]['description'])
        print()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
                        prog="extraire_flux_feedparser.py",
                        description="Extrait le texte d'un fichier XML")

    parser.add_argument("xml_file")
    args = parser.parse_args()

    # le nom du fichier est accessible par args.xml_file

    chemin = "../corpus/" + args.xml_file
    methode_feedparser(chemin)
